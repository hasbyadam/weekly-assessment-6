function sumOfCubes(arr) {
  let res = 0;
  if (arr.length === 0) return res;

  arr.forEach((el) => {
    res += el * el * el;
  });
  return res;
}

console.log(sumOfCubes([3, 4, 5]));
console.log(sumOfCubes([2]));
console.log(sumOfCubes([]));
