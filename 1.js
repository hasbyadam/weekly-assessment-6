function removeString(arr) {
  return arr.filter((el) => {
    return typeof el !== "string";
  });
}

console.log(removeString([1, 2, "a", "b"]));
console.log(removeString([1, "a", "b", 0, 15]));
console.log(removeString([1, 2, "aasf", "1", "123", 123]));
