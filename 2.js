function dropRight(arr, n = 1) {
  let newArr = arr
  for (let i = 0; i < n; i++) {
    newArr.pop()
  }
  return newArr
}

console.log(dropRight([1, 2, 3]))
console.log(dropRight([1, 2, 3], 2))
console.log(dropRight([1, 2, 3], 5))
console.log(dropRight([1, 2, 3], 0))
